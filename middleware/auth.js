export default function ({ store, route, redirect }) {
  if(route.name === 'index'){
  	return redirect('/login')
  }
  if (!store.getters.isAuthenticated && route.name !== 'login') {
    redirect('/login')
  }
  if (store.getters.isAuthenticated && route.name === 'login') {
    redirect('/home')
  }
}