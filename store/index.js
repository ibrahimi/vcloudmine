import Vuex from 'vuex'

const createStore = () => {
    return new Vuex.Store({
        state: () => ({
            user: null,
            datauser:null
        }),
        mutations: {
            setUser (state, user) {
                state.user = user
              },
              setdatauser(state, datauser) {
                state.datauser = datauser
              }
        },
        actions: {
            user(){
                return this.state.user
            },
            datauser(){
                return this.state.datauser
            }
        },
        getters : {
          isAuthenticated (state) {
            return !!state.user
            }
        }
      })
}
export default createStore
