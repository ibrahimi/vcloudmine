import axios from 'axios'
import crypto from 'crypto'
var params={
  pubeu : 'AlVrOGjZhTOXUVjg',
  seceu : 'P5OL4Xy1iKzTZCwf5bXZmGPIo8KQkWgf',
  pubca : 'dObH0YO6zhfsrIsW',
  secca : 'DhQSKEqmpEH0Rwvs3jZ9sbKC24lTAN6K',
  data :{}
}
var baseurl=''
function builurl() {
  var url = 'https://'+params.data.data.endpoint.split("-").pop()+'.api.ovh.com/1.0'+params.data.url
  return url
}
async function ovjs(data){
  params.data=data
  await gettime()
  return requ(data.method,builurl(),data.body)
}
async function setheader()
{
  let header
  if(params.data.data.endpoint.split("-").pop()==='eu'){
    if(params.data.data.consumerKey==''){
      header = {
        'X-Ovh-Application': params.pubeu,
        'X-Ovh-Timestamp': params.timenow + Math.round(Date.now() / 1000)
      }
    }else{
    header = {
      'X-Ovh-Application': params.pubeu,
      'X-Ovh-Timestamp': params.timenow + Math.round(Date.now() / 1000),
      'X-Ovh-Signature': signRequest(),
      'X-Ovh-Consumer' : params.data.data.consumerKey
    }
  }
  }
  if(params.data.data.endpoint.split("-").pop()==='ca'){
    if(params.data.data.consumerKey==''){
      header = {
        'X-Ovh-Application': params.pubca,
        'X-Ovh-Timestamp': params.timenow + Math.round(Date.now() / 1000)
      }
    }else{
    header = {
      'X-Ovh-Application': params.pubca,
      'X-Ovh-Timestamp': params.timenow + Math.round(Date.now() / 1000),
      'X-Ovh-Signature': signRequest(),
      'X-Ovh-Consumer' : params.data.data.consumerKey
    }
  }
  }
  return header
}
async function requ(method,url,data){
    const head = await setheader()
    let cb
    await axios({
      method: method,
      url: url,
      data: data,
      headers:head
    }).then((res)=>{
      cb=res.data
    });
    return cb
}
async function gettime(){
  await axios.get('https://'+params.data.data.endpoint.split("-").pop()+'.api.ovh.com/1.0/auth/time').then((data)=>{
    params['timenow']= data.data - Math.round(Date.now() / 1000)
  });
}

function signRequest() {
  let s
  const bb=JSON.stringify(params.data.body)
  if(params.data.data.endpoint.split("-").pop()=='eu')
  {

  s = [
    params.seceu,
    params.data.data.consumerKey,
    params.data.method,
    builurl(),
    bb || '',
    params.timenow + Math.round(Date.now() / 1000)
  ];
  }else
  {
  s = [
    params.secca,
    params.data.data.consumerKey,
    params.data.method,
    builurl(),
    bb || '',
    params.timenow + Math.round(Date.now() / 1000)
  ];
  }
  console.log(s.join('+'))
  return '$1$' + crypto.createHash('sha1').update(s.join('+')).digest('hex');
}

export const ovh = ovjs
