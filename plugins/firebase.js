import * as firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/database'

var config = {
    apiKey: "AIzaSyCMUWtK5X4y22UDwLG2vO2ua3QyoJI8RoA",
    authDomain: "vueservice-aa158.firebaseapp.com",
    databaseURL: "https://vueservice-aa158.firebaseio.com",
    projectId: "vueservice-aa158",
    storageBucket: "vueservice-aa158.appspot.com",
    messagingSenderId: "330305644553",
    timestampsInSnapshots: false
}

function firebases() {
	// body...
	return firebase.initializeApp(config)
}
export default firebases()