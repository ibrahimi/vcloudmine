import firebase from '~/plugins/firebase.js'
export default function({store,redirect}) { 
    return new Promise((resolve, reject) => {
      firebase.auth().onAuthStateChanged(user => {
        if (user) {
          resolve(store.commit('setUser', user))
          return redirect('/home')
        }
        return resolve()
      })
    })
 }
